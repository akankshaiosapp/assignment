//
//  SingtelAppTests.swift
//  SingtelAppTests
//
//  Created by Akanksha Thakur on 3/7/20.
//  Copyright © 2020 Akanksha Thakur. All rights reserved.
//

import XCTest
@testable import SingtelApp

class SingtelAppTests: XCTestCase {
    var view: ViewController?
    override func setUpWithError() throws {
        view = ViewController()
    }

    override func tearDownWithError() throws {
        view = nil
    }

    func testView() throws {
        // check for labels
        XCTAssertNotNil(view?.label1)
        XCTAssertNotNil(view?.label2)
        view?.label1.text = "step"
        view?.label2.text = "?"
        XCTAssertNotNil(view?.label1.text)
        XCTAssertNotNil(view?.label2.text)
        
        //check random numbers
        view?.items = 12
        view?.randomNumberGenerator()
        XCTAssertNotNil(view?.randomNumbers)
        ///
    }
}

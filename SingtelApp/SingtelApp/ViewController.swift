//
//  ViewController.swift
//  runTest
//
//  Created by Akanksha Thakur on 30/6/20.
//  Copyright © 2020 Akanksha Thakur. All rights reserved.
//

import UIKit
class ViewController: UIViewController, UICollectionViewDelegate {
    var myCollectionView:UICollectionView?
    var items: Int?
    let label1 = UILabel()
    let label2 = UILabel()
    var arrayView1: [UIView] = []
    var arrayView2: [UIView] = []
    //random number
       var randomNumbers: [Int] = []
       var randomNumbersDup: [Int] = []
       //
    
    var countSelections: Int = 0 {
        didSet {
            label2.text = String(countSelections)
        }
    }
    var checkCount = 0
    var count = 0
    
    var prevSelected: [Int] = []
    var matchedSelectedIndex:[IndexPath] = []
    /// Show alert when the matching is complete.
    var matchedSelectedNumber: [Int] = [] {
        didSet {
            if matchedSelectedNumber.count == items {
                var alert = UIAlertController(title: "Congratulations", message: "You have completed in \(countSelections) steps.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    self.randomNumbers.removeAll()
                    self.randomNumbersDup.removeAll()
                    self.randomNumberGenerator()
                    ///
                    self.arrayView1.removeAll()
                    self.arrayView2.removeAll()
                    self.myCollectionView?.reloadData()
                    self.countSelections = 0
                    self.matchedSelectedNumber.removeAll()
                    self.lastSelectedIndex = nil
                    self.lastSelectedNumber = nil
                  }))
                present(alert, animated: true, completion: nil)
            }
        }
    }
    var newSelectedNumber: Int = 0
    //
    var lastSelectedIndex: IndexPath?
    var lastSelectedNumber: Int?
    
    ///
    var flip = 0
    
    fileprivate func setButtonStyle(_ button: UIButton) {
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOffset = CGSize(width: 0.0, height: 0.1)
        button.layer.shadowRadius = 0.3
        button.layer.shadowOpacity = 0.2
        button.layer.cornerRadius = 6.0
        button.layer.borderWidth = 0.6
        button.layer.borderColor = UIColor.gray.cgColor
        button.showsTouchWhenHighlighted = true
        button.backgroundColor = UIColor.black.withAlphaComponent(0.1)
    }
    
    fileprivate func setLabelStyle() {
        ///
        label1.text = "Step:"
        label2.text = "0"
        label2.textColor = .blue
        label1.font = UIFont.boldSystemFont(ofSize: 16)
        label2.font = UIFont.boldSystemFont(ofSize: 18)
    }
    
     func randomNumberGenerator() {
        ///
        ///Random numbers
        ///
        let values = Int((items ?? 0) / 2)
        for _ in 1...values {
            let randomInt = Int.random(in: 1...100)
            randomNumbers.append(randomInt)
            randomNumbersDup.append(randomInt)
        }
        randomNumbers.append(contentsOf: randomNumbersDup)
        randomNumbers = randomNumbers.shuffled()
    }
    
    fileprivate func setLabelConstraints(_ topView: UIView, _ button: UIButton) {
        label2.translatesAutoresizingMaskIntoConstraints = false
        label2.leftAnchor.constraint(equalTo: label1.rightAnchor, constant: 5).isActive = true
        label2.trailingAnchor.constraint(equalTo: topView.trailingAnchor, constant: -20).isActive = true
        label2.topAnchor.constraint(equalTo: topView.topAnchor, constant: 20).isActive = true
        label2.bottomAnchor.constraint(equalTo: topView.bottomAnchor, constant: -20).isActive = true
        label1.sizeToFit()
        label2.sizeToFit()
        label1.setContentHuggingPriority(UILayoutPriority.defaultLow, for:.horizontal)
        label2.setContentHuggingPriority(UILayoutPriority.defaultHigh, for:.horizontal)
        label1.textAlignment = .right
        label2.textAlignment = .center
        
        //label1  constraints
        label1.translatesAutoresizingMaskIntoConstraints = false
        label1.leftAnchor.constraint(equalTo: button.rightAnchor, constant: 20).isActive = true
        label1.topAnchor.constraint(equalTo: topView.topAnchor, constant: 20).isActive = true
        label1.bottomAnchor.constraint(equalTo: topView.bottomAnchor, constant: -20).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        countSelections = 0
        prevSelected.removeAll()
        //
        let viewN = UIView()
        let topView = UIView()
        let button = UIButton()
        setButtonStyle(button)
        setLabelStyle()
        ///
        ///Calculate number of items on the screen based on given frame.
        let height = (self.view.frame.height - 40 - 50 - 100)
        let width = (self.view.frame.width - 40 - 40)
        let value1: Int = Int(width / (width / 3))
        let value2: Int = Int(height / (height / 4))
        items = value1 * value2
        //
        topView.frame = CGRect(x: 20, y: 20, width: self.view.bounds.size.width - 40, height: 100)
        button.frame = CGRect(x: 20, y: 20, width: 80, height: 30)
        button.setTitle("Restart", for: .normal)
        button.tintColor = .black
        button.setTitleColor(.blue, for: .normal)
        //button.backgroundColor = .yellow
        topView.addSubview(button)
        viewN.addSubview(topView)
        viewN.backgroundColor = .white
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: ((self.view.frame.width - 40 - 40)/3), height: ((self.view.frame.height - 40 - 50 - 100)/4))
        
        myCollectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        myCollectionView?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "MyCell")
        myCollectionView?.backgroundColor = UIColor.white
        viewN.addSubview(myCollectionView ?? UICollectionView())
        self.view = viewN
        //Collection View Constraints
        myCollectionView?.translatesAutoresizingMaskIntoConstraints = false
        myCollectionView?.leadingAnchor.constraint(equalTo: viewN.leadingAnchor, constant: 20).isActive = true
        myCollectionView?.trailingAnchor.constraint(equalTo: viewN.trailingAnchor, constant: -20).isActive = true
        myCollectionView?.topAnchor.constraint(equalTo: viewN.topAnchor, constant: 100).isActive = true
        myCollectionView?.bottomAnchor.constraint(equalTo: viewN.bottomAnchor, constant: -20).isActive = true
        //
        //Collection View delegate
        myCollectionView?.dataSource = self
        myCollectionView?.delegate = self
        //
        //label Constraints
        topView.addSubview(label2)
        topView.addSubview(label1)
        setLabelConstraints(topView, button)
        self.view.updateConstraintsIfNeeded()
        randomNumberGenerator()
        ///
        
    }
    @objc func buttonAction(sender: UIButton!) {
      print("Button tapped")
        randomNumbers.removeAll()
        randomNumbersDup.removeAll()
        randomNumberGenerator()
        ///
        arrayView1.removeAll()
        arrayView2.removeAll()
        self.myCollectionView?.reloadData()
        self.countSelections = 0
        self.matchedSelectedNumber.removeAll()
        self.lastSelectedIndex = nil
        self.lastSelectedNumber = nil
    }
}


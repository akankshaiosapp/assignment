
//  Created by Akanksha Thakur on 3/7/20.
//  Copyright © 2020 Akanksha Thakur. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items ?? 0
    }
    /// Set constraints programmatically to labels and views.
    fileprivate func setConstraints(_ labelFirst: UILabel, _ mediumFirstView: UIView, _ labelSecond: UILabel, _ mediumSecondView: UIView, _ myCell: UICollectionViewCell) {
        ///
        labelFirst.translatesAutoresizingMaskIntoConstraints = false
        labelFirst.leftAnchor.constraint(equalTo: mediumFirstView.leftAnchor, constant: 0).isActive = true
        labelFirst.trailingAnchor.constraint(equalTo: mediumFirstView.trailingAnchor, constant: 0).isActive = true
        labelFirst.topAnchor.constraint(equalTo: mediumFirstView.topAnchor, constant: 0).isActive = true
        labelFirst.bottomAnchor.constraint(equalTo: mediumFirstView.bottomAnchor, constant: 0).isActive = true
        ///
        labelSecond.translatesAutoresizingMaskIntoConstraints = false
        labelSecond.leftAnchor.constraint(equalTo: mediumSecondView.leftAnchor, constant: 0).isActive = true
        labelSecond.trailingAnchor.constraint(equalTo: mediumSecondView.trailingAnchor, constant: 0).isActive = true
        labelSecond.topAnchor.constraint(equalTo: mediumSecondView.topAnchor, constant: 0).isActive = true
        labelSecond.bottomAnchor.constraint(equalTo: mediumSecondView.bottomAnchor, constant: 0).isActive = true
        ///
        mediumSecondView.translatesAutoresizingMaskIntoConstraints = false
        mediumSecondView.leftAnchor.constraint(equalTo: myCell.leftAnchor, constant: 0).isActive = true
        mediumSecondView.trailingAnchor.constraint(equalTo: myCell.trailingAnchor, constant: 0).isActive = true
        mediumSecondView.topAnchor.constraint(equalTo: myCell.topAnchor, constant: 0).isActive = true
        mediumSecondView.bottomAnchor.constraint(equalTo: myCell.bottomAnchor, constant: 0).isActive = true
        ///
        mediumFirstView.translatesAutoresizingMaskIntoConstraints = false
        mediumFirstView.leftAnchor.constraint(equalTo: myCell.leftAnchor, constant: 0).isActive = true
        mediumFirstView.trailingAnchor.constraint(equalTo: myCell.trailingAnchor, constant: 0).isActive = true
        mediumFirstView.topAnchor.constraint(equalTo: myCell.topAnchor, constant: 0).isActive = true
        mediumFirstView.bottomAnchor.constraint(equalTo: myCell.bottomAnchor, constant: 0).isActive = true
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath)
        ///Set cell Style
        myCell.layer.borderColor = UIColor.black.cgColor
        myCell.layer.borderWidth = 1.0
        myCell.layer.masksToBounds = true
        myCell.layer.cornerRadius = 8.0
        myCell.backgroundColor = UIColor.blue
        myCell.isUserInteractionEnabled = true
        ///
        /// Create view to add number labels and flip the views.
        let mediumSecondView = UIView()
        let mediumFirstView = UIView()
        ///
        ///Set Style
        mediumFirstView.layer.masksToBounds = true
        mediumFirstView.layer.cornerRadius = 8.0
        mediumSecondView.layer.masksToBounds = true
        mediumSecondView.layer.cornerRadius = 8.0
        ///
        /// Create labels
        let labelFirst = UILabel()
        let labelSecond = UILabel()
        labelFirst.textAlignment = .center
        labelSecond.textAlignment = .center
        labelFirst.text = "?"
        ///
        labelSecond.text = String(randomNumbers[indexPath.row])
        ///
        mediumFirstView.addSubview(labelFirst)
        mediumSecondView.addSubview(labelSecond)
        ///
        myCell.addSubview(mediumSecondView)
        myCell.addSubview(mediumFirstView)
        ///
        arrayView1.append(mediumFirstView)
        arrayView2.append(mediumSecondView)
        ///
        mediumFirstView.backgroundColor = .blue
        mediumSecondView.backgroundColor = UIColor.orange
        setConstraints(labelFirst, mediumFirstView, labelSecond, mediumSecondView, myCell)
        return myCell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //For entire screen size
        let screenSize = UIScreen.main.bounds.size
        return screenSize
    }
    
    fileprivate func flipBack(_ index: IndexPath) {
        UIView.transition(from: self.arrayView2[index.row], to: self.arrayView1[index.row],
                          duration: 0.4, options: [.transitionFlipFromLeft,
                                                   .showHideTransitionViews]) { _ in
                                                    
        }
    }
    
    fileprivate func flipFront(_ indexPath: IndexPath) {
        UIView.transition(from: arrayView1[indexPath.row], to: arrayView2[indexPath.row],
                          duration: 0.4, options: [.transitionFlipFromRight,
                                                   .showHideTransitionViews]) { _ in
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if countSelections == 0 || lastSelectedIndex == nil {
            guard let view  = collectionView.cellForItem(at: indexPath) else { return }
            view.isUserInteractionEnabled = false
            lastSelectedNumber = self.randomNumbers[indexPath.row]
            lastSelectedIndex = indexPath
            countSelections += 1
            flipFront(indexPath)
        } else {
            guard let view  = collectionView.cellForItem(at: indexPath) else { return }
            view.isUserInteractionEnabled = false
            flipFront(indexPath)
            
            if lastSelectedNumber == self.randomNumbers[indexPath.row] {
                if let lastIndex = lastSelectedIndex {
                    matchedSelectedIndex.append(lastIndex)
                }
                matchedSelectedIndex.append(indexPath)
                matchedSelectedNumber.append(self.randomNumbers[indexPath.row])
                matchedSelectedNumber.append(self.randomNumbers[indexPath.row])
                lastSelectedIndex = nil
                lastSelectedNumber = nil
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    guard let view  = collectionView.cellForItem(at: indexPath) else { return }
                    view.isUserInteractionEnabled = true
                    self.flipBack(indexPath)
                    if let lastIndex = self.lastSelectedIndex {
                        self.lastSelectedIndex = nil
                        self.lastSelectedNumber = nil
                        guard let view  = collectionView.cellForItem(at: lastIndex) else { return }
                        view.isUserInteractionEnabled = true
                        self.flipBack(lastIndex)
                    }
                }
                
            }
        }
        
    }
    
}
